-- 2008 - 8_OrgProfile
select 
   NVL((select ADMINID from S_VT_DISTRICT_S where rownum = 1),0) AS ADMINID,    
   S_VT_SCHOOLS_X.ENRORGID AS ENRORGID,
    TO_CHAR(STARTDATE,'MM/DD/YYYY') AS SYTFRAMEBEG,
    TO_CHAR(ENDDATE,'MM/DD/YYYY') AS SYTFRAMEEND,
    (select count(*) from calendar_day where schoolid = schools.school_number and insession = 1 and date_value between S_VT_SCHOOLS_X.STARTDATE AND S_VT_SCHOOLS_X.ENDDATE
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Saturday' 
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Sunday'
    ) as SESSDAYS,
    (select count(*) from calendar_day where schoolid = schools.school_number and insession = 1 and b = 1 and date_value between S_VT_SCHOOLS_X.STARTDATE AND S_VT_SCHOOLS_X.ENDDATE
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Saturday' 
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Sunday'
    ) as KP_AM_SESS,
    (select count(*) from calendar_day where schoolid = schools.school_number and insession = 1 and c = 1 and date_value between S_VT_SCHOOLS_X.STARTDATE AND S_VT_SCHOOLS_X.ENDDATE
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Saturday' 
      AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Sunday'
    ) as KP_PM_SESS,    
   NVL((select SYTFRAME from S_VT_DISTRICT_S where rownum = 1),0) AS SYTFRAME,
   NVL((select SY from S_VT_DISTRICT_S where rownum = 1),0) AS SY
FROM S_VT_SCHOOLS_X 
inner join schools on S_VT_schools_x.schoolsdcid = schools.dcid