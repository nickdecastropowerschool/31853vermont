--2009 - 8a_OrgProfile_Holidays
select 
    NVL((select ADMINID from S_VT_DISTRICT_S where rownum = 1),0) AS ADMINID,    
    S_VT_SCHOOLS_X.ENRORGID AS ORGID,
    TO_CHAR( CALENDAR_DAY.DATE_VALUE, 'MM/DD/YYYY') AS HOLIDAY,    
    NVL((select SY from S_VT_DISTRICT_S where rownum = 1),0) AS SY 
FROM CALENDAR_DAY 
join schools on calendar_day.schoolid = schools.SCHOOL_NUMBER
inner join S_VT_SCHOOLS_X ON SCHOOLS.DCID = S_VT_SCHOOLS_X.SCHOOLSDCID
WHERE SCHOOLS.STATE_EXCLUDEFROMREPORTING  = 0
  AND calendar_day.INSESSION=0 
  AND  calendar_day.date_VALUE >= S_VT_SCHOOLS_X.STARTDATE
  AND  calendar_day.date_VALUE <= S_VT_SCHOOLS_X.ENDDATE
  AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Saturday' 
  AND to_char (calendar_day.date_VALUE, 'FmDay', 'nls_date_language=english')<>'Sunday'