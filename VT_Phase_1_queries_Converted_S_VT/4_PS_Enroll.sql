--2005 - 4_PS_Enroll
select  
	NVL((select ADMINID from S_VT_DISTRICT_S where rownum = 1),0) AS ADMINID,    
	S_VT_SCHOOLS_X.ENRORGID AS ENRORGID,
	STUDENTS.STATE_STUDENTNUMBER as PERMNUMBER,
	decode(STUDENTS.FEDETHNICITY,1,1,2) as ETHNO,
    CASE when (select count(*) from studentrace where studentid = students.id and racecd = 'I') > 0 then 'Y' else 'N' END as RACE_AMI,
    CASE when (select count(*) from studentrace where studentid = students.id and racecd = 'A') > 0 then 'Y' else 'N' END as RACE_ASI,
    CASE when (select count(*) from studentrace where studentid = students.id and racecd = 'B') > 0 then 'Y' else 'N' END as RACE_AFA,
    CASE when (select count(*) from studentrace where studentid = students.id and racecd = 'P') > 0 then 'Y' else 'N' END as RACE_NAT,
    CASE when (select count(*) from studentrace where studentid = students.id and racecd = 'W') > 0 then 'Y' else 'N' END as RACE_WHT,
	S_VT_STU_X.ADMINSTAT as ADMINSTAT,
    (select ENTRYCODE from PS_ENROLLMENT_ALL r1 where r1.studentid = students.id and r1.schoolid = students.schoolid and rownum =1
      and r1.ENTRYDATE = (SELECT MIN(ENTRYDATE) FROM PS_ENROLLMENT_ALL r2 where r1.studentid = r2.studentid and r1.schoolid = r2.schoolid AND r2.entrycode is not null))
    AS ENTRYTYPE,    
    case when 
    (select ENTRYDATE from PS_ENROLLMENT_ALL r1 where r1.studentid = students.id and r1.schoolid = students.schoolid and rownum =1
      and r1.ENTRYDATE = (SELECT MIN(ENTRYDATE) FROM PS_ENROLLMENT_ALL r2 where r1.studentid = r2.studentid and r1.schoolid = r2.schoolid AND r2.entrycode is not null))    
       >= S_VT_SCHOOLS_X.STARTDATE 
      THEN 
      (select to_char(ENTRYDATE, 'MM/DD/YYYY') from PS_ENROLLMENT_ALL r1 where r1.studentid = students.id and r1.schoolid = students.schoolid and rownum =1
      and r1.ENTRYDATE = (SELECT MIN(ENTRYDATE) FROM PS_ENROLLMENT_ALL r2 where r1.studentid = r2.studentid and r1.schoolid = r2.schoolid AND r2.entrycode is not null))    
    ELSE to_char(S_VT_SCHOOLS_X.STARTDATE,'MM/DD/YYYY') 
    END as ENRBEGDATE,
    to_char(S_VT_SCHOOLS_X.STARTDATE,'MM/DD/YYYY') ,    
    CASE 
        WHEN students.EXITDATE > CURRENT_DATE THEN  null
        ELSE to_char(STUDENTS.EXITDATE,'MM/DD/YYYY')
    END as ENRENDDATE, 
    CASE 
        WHEN students.EXITDATE > CURRENT_DATE THEN  null
        ELSE STUDENTS.EXITCODE         
	END as EXITTYPE, 
	null as NEWENRORGID,		
	DECODE(S_VT_STU_X.SS504,'Y','Y','N') as SS504,
	DECODE(S_VT_STU_X.EST,'Y','Y','N') as EST,
	S_VT_STU_X.NSLELG,  
	DECODE(S_VT_STU_X.HOMELESS,'Y','Y','N') as HOMELESS,
	S_VT_STU_X.MKSERVICE, 
	S_VT_STU_X.UNYOUTH,
	S_VT_STU_X.NTRESNO,
	DECODE(S_VT_STU_X.DUAL_ENR,'Y','Y','N') as DUAL_ENR,
	DECODE(S_VT_STU_X.T1SERVICES,'Y' ,'Y','N') as T1SERVICES,
	DECODE(S_VT_STU_X.IST1RLA,'Y' ,'Y','N') as IST1RLA,
	DECODE(S_VT_STU_X.IST1MATH,'Y' ,'Y','N') as IST1MATH,
	DECODE(S_VT_STU_X.IST1SCIENCE,'Y' ,'Y','N') as IST1SCIENCE,
	DECODE(S_VT_STU_X.IST1SOCIAL,'Y' ,'Y','N') as IST1SOCIAL,
	DECODE(S_VT_STU_X.IST1VOC,'Y' ,'Y','N') as IST1VOC,
	DECODE(S_VT_STU_X.IST1OTHER,'Y' ,'Y','N') as IST1OTHER,
	DECODE(S_VT_STU_X.SST1HEALTH,'Y','Y','N') as SST1HEALTH,
	DECODE(S_VT_STU_X.SST1GUID,'Y' ,'Y','N') as SST1GUID,
	DECODE(S_VT_STU_X.SST1OTHER,'Y' ,'Y','N') as SST1OTHER,
	NVL((select SY from S_VT_DISTRICT_S where rownum = 1),0) AS SY 
from PS_ENROLLMENT_ALL 
inner join students ON students.id  = PS_ENROLLMENT_ALL.studentid and PS_ENROLLMENT_ALL.SCHOOLID = students.schoolid
inner join schools on students.schoolid = schools.school_number
inner join S_VT_SCHOOLS_X ON SCHOOLS.DCID = S_VT_SCHOOLS_X.SCHOOLSDCID 
left join S_VT_STU_X on students.dcid = S_VT_STU_X.StudentsDCID
WHERE SCHOOLS.STATE_EXCLUDEFROMREPORTING  = 0
  AND STUDENTS.STATE_EXCLUDEFROMREPORTING = 0
  AND  PS_ENROLLMENT_ALL.exitdate >= S_VT_SCHOOLS_X.STARTDATE
  AND  PS_ENROLLMENT_ALL.entrydate <= S_VT_SCHOOLS_X.ENDDATE
ORDER BY STUDENTS.STATE_STUDENTNUMBER,STUDENTS.STUDENT_NUMBER   
  
  
  
  
