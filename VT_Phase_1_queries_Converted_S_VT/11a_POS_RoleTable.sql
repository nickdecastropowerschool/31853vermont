--2001 - 11a_POS_RoleTable
select DISTINCT
    NVL((select ADMINID from S_VT_DISTRICT_S where rownum = 1),0) AS ADMINID,    
    S_VT_USR_X.EMPORGID,
	S_VT_SCHOOLS_X.POSID,
	S_VT_USR_X.EDUCATORID,
	S_VT_POSROLE_S.ROLEID,
	S_VT_POSROLE_S.ROLEPERCENT,
	NVL((select SY from S_VT_DISTRICT_S where rownum = 1),0) AS SY
from S_VT_POSROLE_S 
inner join SCHOOLSTAFF on S_VT_POSROLE_S.SCHOOLSTAFFDCID = SCHOOLSTAFF.DCID
inner join S_VT_USR_X on USERS.DCID = S_VT_USR_X.USERSDCID
inner join S_VT_SCHOOLS_X on SCHOOLS.DCID = S_VT_SCHOOLS_X.SCHOOLSDCID   
order by S_VT_USR_X.EDUCATORID